MapaConceptual 1

@startmindmap
* Industria 4.0
	* Evolucion Historica
		*** Industria 1.0 (1784) 
                    **** Produccion mecanica
                    **** Equipos vasados en energia a vapor y energía hidraulica
		*** Industria 2.0 (1870)
                    **** Linea de ensamble
                    **** Energia electrica
                    **** Producción en masa
                *** Industria 3.0 (1969)
                    **** Produccion automatizada utilizando electronica yTI
                    **** Programmable Logic Controller (PLC)
                *** Industria 4.0 (Ahora)
                    **** Produccion inteligente con con decisión autonoma
                         ***** IoT
                         ***** igData
                         ***** Cloud
                         ***** Robot
                         ***** etc
	* Cambios
            *** Integracion de Tic's
                **** Procesos automatizados
                     ***** Produccion, manejo de inventarios, entrega de productos.
                **** Reducción de puestos de trabajos
                **** Aparición de nuevas profesiones 
            *** Transformacion de las "Empresas de manofactura" en "Empresas de Tic's"
                     **** Cada vez existe menos diferenciación entre las industrias
            *** Nuevos paradigmas y tecnologias
                     **** Paradigma
                          ***** Velocidad
                     **** Negocios basados en plataformas
                     **** Inteligencia artificial
            *** Nuevas culturas digitales
            
                     **** Phono Sapiens
                     **** Youtuber
                     **** e-sports
@endmindmap


MapaConceptual 2

@startmindmap
* México y la Industria 4.0

	* Intereses
		*** Aprovechar la nuevas tecnologias 

		*** Optimizar costos
                    **** Recorte de costos
                    **** Optimización y generación de varias areas de negocio
      
                    *** Hacer más e invertir menos           
	* Sistemas Automatizados
    
            *** PLC
             **** Micro controlador que ordena a una maquina comó funcionar.
            *** Servicios en la nuve
                     **** entrenamientos y herramientas para hacelerar procesos de creación
                     
	* Ruta de dessarrollo
            *** Computarización
            
                **** Las tareas son compatibles con el sistema de procesamiento de datos
                **** Los empleados se alivian de las acividades manuales repetitivas
                
            *** Conectividad
            
                **** Los sistemas de procesamientos de datos estan estructurados y vinculados
                **** Los procesamientos comerciales principales se reflejan los sistemas TI
                
            *** Visibilidad
            
                **** Las empresas tienen una transparencia total sobre sus procesos de valor agregado en forma de sombra digital
                **** La dirección toma desiciones basadas en datos.
                
            *** Transparencia
            
                **** Las empresas entienden por que ocurren los eventos
                **** El conocimiento se descubre a traves del reconocimiento
                
            *** Previsibilidad
            
                **** Las empresas saben lo que sucederá en el futuro
                **** Las desiciones se toman sobre la base de escenarios futuros
                
            *** Adaptabilidad     
                **** Las empresas reaccionan de forma autónoma y es totalmente viable.  
                          
@endmindmap














